export const showFlags = () => {
  return {
    type: "SHOWFLAGS",
  };
};

export const hideFlags = () => {
  return {
    type: "HIDEFLAGS",
  };
};

export const showPhones = () => {
  return {
    type: "SHOWPHONES",
  };
};

export const hidePhones = () => {
  return {
    type: "HIDEPHONES",
  };
};

export const selectCountry = (data) => {
  return {
    type: "SELECTCOUNTRY",
    payload: data,
  };
};

export const selectPhone = (data) => {
  return {
    type: "SELECTPHONE",
    payload: data,
  };
};

export const handleUserChange = (data) => {
  let value = data.replace(/[^A-Za-z]/gi, "");

  if (value.length < 4) {
    return {
      type: "USER_FAIL",
      payload: {
        isValid: false,
        value: value,
        errorText: "მომხმარებლის სახელი უნდა შეიცავდეს მინიმუმ 4 სიმბოლოს",
      },
    };
  } else {
    return {
      type: "USER_SUCCESS",
      payload: {
        isValid: true,
        value: value,
        errorText: "",
      },
    };
  }
};

export const handlePwChange = (data) => {
  if (data.length < 6) {
    return {
      type: "PW_FAIL",
      payload: {
        isValid: false,
        value: data,
        errorText: "პაროლი უნდა შედგებოდეს მინიმუმ 6 სიმბოლოსგან",
      },
    };
  } else {
    return {
      type: "PW_SUCCESS",
      payload: {
        isValid: true,
        value: data,
        errorText: "",
      },
    };
  }
};

export const handleFnameChange = (data) => {
  let value = data.replace(/[^A-Za-z]/gi, "");

  if (data === "" || data.length < 2) {
    return {
      type: "FNAME_FAIL",
      payload: {
        isValid: false,
        value: value,
        errorText: "სახელი უნდა შეიცავდეს მინიმუმ 2 სიმბოლოს",
      },
    };
  } else {
    return {
      type: "FNAME_SUCCESS",
      payload: {
        isValid: true,
        value: value,
        errorText: "",
      },
    };
  }
};

export const handleLnameChange = (data) => {
  let value = data.replace(/[^A-Za-z]/gi, "");

  if (data === "" || data.length < 4) {
    return {
      type: "LNAME_FAIL",
      payload: {
        isValid: false,
        value: value,
        errorText: "გვარი უნდა შეიცავდეს მინიმუმ 4 სიმბოლოს",
      },
    };
  } else {
    return {
      type: "LNAME_SUCCESS",
      payload: {
        isValid: true,
        value: value,
        errorText: "",
      },
    };
  }
};

export const handlePidChange = (data) => {
  let value = data.replace(/\D/g, "");

  if (value.length < 11) {
    return {
      type: "PID_FAIL",
      payload: {
        isValid: false,
        value: value,
        errorText: "პირადი ნომერი უნდა შეიცავდეს მინიმუმ 11 ციფრს",
      },
    };
  } else {
    return {
      type: "PID_SUCCESS",
      payload: {
        isValid: true,
        value: value,
        errorText: "",
      },
    };
  }
};

export const handleNumChange = (data) => {
  let value = data.replace(/\D/g, "");

  if (value.length < 9) {
    return {
      type: "NUM_FAIL",
      payload: {
        isValid: false,
        value: value,
        errorText: "ტელეფონი უნდა შეიცავდეს მინიმუმ 9 ციფრს",
      },
    };
  } else {
    return {
      type: "NUM_SUCCESS",
      payload: {
        isValid: true,
        value: value,
        errorText: "",
      },
    };
  }
};

export const handleEmailChange = (data) => {
  const emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const textEmail = emailReg.test(String(data).toLowerCase());

  if (textEmail === false && data !== "") {
    return {
      type: "EMAIL_FAIL",
      payload: {
        isValid: false,
        value: data,
        errorText: "მითითებული იმეილი არასწორია",
      },
    };
  } else {
    return {
      type: "EMAIL_SUCCESS",
      payload: {
        isValid: true,
        value: data,
        errorText: "",
      },
    };
  }
};

export const handleFormValidation = (data) => {
  const firstItem = data[0];
  const allEqual = data.every((val, i, arr) => val === arr[0]);
  if (firstItem === true && allEqual === true) {
    return {
      type: "VALIDATION_SUCCESS",
    };
  } else {
    return {
      type: "VALODATION_FAIL",
    };
  }
};

export const handleModalClose = () => {
  return {
    type: "MODAL_CLOSE",
  };
};
