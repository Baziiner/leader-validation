import React from "react";
import "./MainContainer.scss";
import Input from "../Input";
import { useSelector } from "react-redux";
import Modal from "../Modal";
import AdBanner from "../../assets/images/reg_binus_ban.jpg";

const MainContainer = () => {
  const showModal = useSelector((state) => state.input.showModal);

  return (
    <div className="MainContainer">
      {showModal && <Modal />}
      <div className="MainContainer-content">
        <div className="content-top">
          <h1>რეგისტრაცია</h1>
          <p className="highlight">
            სავალდებულოა ყველა ველი შეივსოს კორექტულად მხოლოდ ლათინური ასოებით
            და ციფრებით.
          </p>
          <p>
            <b>ყურადღება:</b> რეგისტრაციისას მითითებული პირადი მონაცემების
            უზუსტობის შემთხვევაში, კომპანია იტოვებს უფლებას შეგიზღუდოთ საიტით
            სარგებლობა!
          </p>
        </div>
        <div className="content-main">
          <div className="field-wrapper">
            <Input type="fname" title="სახელი" required name="fname" />
            <Input type="lname" title="გვარი" required name="lname" />
            <Input type="country" title="ქვეყანა" required name="country" />
            <Input type="pid" title="პირადი ნომერი" required name="pid" />
            <Input type="date" title="დაბადების თარიღი" required name="date" />
            <Input type="phone" title="ტელეფონი" required name="phone" />
            <Input type="email" title="ელ-ფოსტა" name="email" />
            <ul className="divider_line"></ul>
            <Input type="text" title="მომხმარებელი" required name="username" />
            <Input type="password" title="პაროლი" required name="password" />
            <Input type="button" />
          </div>
          <div className="field-wrapper">
            <img src={AdBanner} alt="ad-banner" />
          </div>
        </div>
        <div className="rule-text">
          რეგისტრაციის ღილაკზე დაჭერით <br />
          ვადასტურებ, რომ ვარ 18 წლის და ვეთანხმები საიტის წესებს და პირობებს
        </div>
      </div>
    </div>
  );
};
export default MainContainer;
