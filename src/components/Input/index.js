import React from "react";
import "./Input.scss";
import { useSelector, useDispatch } from "react-redux";
import {
  showFlags,
  hideFlags,
  showPhones,
  hidePhones,
  selectCountry,
  selectPhone,
  handleUserChange,
  handlePwChange,
  handleLnameChange,
  handleFnameChange,
  handlePidChange,
  handleNumChange,
  handleEmailChange,
  handleFormValidation,
} from "../../actions";

const Input = ({ type, title, required, name }) => {
  const dispatch = useDispatch();
  const options = useSelector((state) => state.input.date);
  const countries = useSelector((state) => state.input.flags);
  const phoneIndex = useSelector((state) => state.input.flagsTel);
  const visibleFlag = useSelector((state) => state.input.flagsVisible);
  const visiblePhone = useSelector((state) => state.input.phoneFlagsVisible);
  const flag = useSelector((state) => state.input.selectedFlag);
  const selectedPhone = useSelector((state) => state.input.selectedPhone);
  const userData = useSelector((state) => state.input.usename);
  const pwData = useSelector((state) => state.input.password);
  const lNameData = useSelector((state) => state.input.lName);
  const fNameData = useSelector((state) => state.input.fName);
  const pidData = useSelector((state) => state.input.pid);
  const numData = useSelector((state) => state.input.phoneNum);
  const emailData = useSelector((state) => state.input.email);

  const handleFlagClick = () => {
    if (visibleFlag) {
      dispatch(hideFlags());
    } else {
      dispatch(showFlags());
    }
  };

  const handlePhoneClick = () => {
    if (visiblePhone) {
      dispatch(hidePhones());
    } else {
      dispatch(showPhones());
    }
  };

  const handleCountrySelect = (data) => {
    dispatch(selectCountry(data));
  };

  const handlePhoneSelect = (data) => {
    dispatch(selectPhone(data));
  };

  const handleUserName = (data) => {
    dispatch(handleUserChange(data));
  };

  const handlePwData = (data) => {
    dispatch(handlePwChange(data));
  };

  const handleFnameData = (data) => {
    dispatch(handleFnameChange(data));
  };

  const handleLnameData = (data) => {
    dispatch(handleLnameChange(data));
  };

  const handlePidData = (data) => {
    dispatch(handlePidChange(data));
  };

  const handleNumData = (data) => {
    dispatch(handleNumChange(data));
  };

  const handleEmailData = (data) => {
    dispatch(handleEmailChange(data));
  };

  const handleDataValidation = (
    emData,
    numData,
    pidData,
    lNameData,
    fNameData,
    pwData,
    userData
  ) => {
    dispatch(handleEmailChange(emData.value));
    dispatch(handleNumChange(numData.value));
    dispatch(handlePidChange(pidData.value));
    dispatch(handleLnameChange(lNameData.value));
    dispatch(handleFnameChange(fNameData.value));
    dispatch(handlePwChange(pwData.value));
    dispatch(handleUserChange(userData.value));
    dispatch(
      handleFormValidation([
        emData.isValid,
        numData.isValid,
        pidData.isValid,
        lNameData.isValid,
        fNameData.isValid,
        pwData.isValid,
        userData.isValid,
      ])
    );
  };

  const textInput = () => {
    switch (type) {
      case "text":
        return (
          <div className="Input input-wrapper">
            {title !== "" ? (
              <label className="input-title" htmlFor={name}>
                {title} {required ? <span>*</span> : ""}
              </label>
            ) : (
              ""
            )}
            <input
              className={`${userData.isValid === false ? "-false" : ""}`}
              pattern="[A-Za-z]"
              type="text"
              value={userData.value}
              onChange={(e) => handleUserName(e.target.value)}
              name={name}
              id={name}
              data-name={title ? title : ""}
            />
            {userData.isValid === false && (
              <div className="error-tooltip">
                <div className="error-text">{userData.errorText}</div>
              </div>
            )}
            {userData.isValid === true && <div className="valid-success" />}
          </div>
        );
      case "password":
        return (
          <div className="Input input-wrapper">
            {title !== "" ? (
              <label className="input-title" htmlFor={name}>
                {title} {required ? <span>*</span> : ""}
              </label>
            ) : (
              ""
            )}
            <input
              className={`${pwData.isValid === false ? "-false" : ""}`}
              type="password"
              name={name}
              id={name}
              value={pwData.value}
              data-name={title ? title : ""}
              onChange={(e) => handlePwData(e.target.value)}
            />
            {pwData.isValid === false && (
              <div className="error-tooltip">
                <div className="error-text">{pwData.errorText}</div>
              </div>
            )}
            {pwData.isValid === true && <div className="valid-success" />}
          </div>
        );
      case "country":
        return (
          <div className="Input input-wrapper">
            {title !== "" ? (
              <label className="input-title" htmlFor={name}>
                {title} {required ? <span>*</span> : ""}
              </label>
            ) : (
              ""
            )}
            <input
              pattern="[A-Za-z]"
              type="text"
              name={name}
              id={name}
              data-name={title ? title : ""}
              value={flag.name || ""}
              onClick={() => handleFlagClick()}
              autoComplete="off"
              readOnly
            />
            <div
              className="selected-flag"
              style={{ backgroundPosition: flag.position }}
            />
            <div className="selected-country -dropdown" />
            {visibleFlag ? (
              <ul className="dropdown-content country-list">
                {countries.map((el, i) => (
                  <li
                    value={el.name}
                    key={i}
                    onClick={() =>
                      handleCountrySelect({
                        name: el.name,
                        short: el.short,
                        position: el.position,
                      })
                    }
                  >
                    <div
                      className={`flag -${el.short}`}
                      style={{ backgroundPosition: el.position }}
                    ></div>
                    {el.name}
                  </li>
                ))}
              </ul>
            ) : (
              ""
            )}
          </div>
        );
      case "fname":
        return (
          <div className="Input input-wrapper">
            {title !== "" ? (
              <label className="input-title" htmlFor={name}>
                {title} {required ? <span>*</span> : ""}
              </label>
            ) : (
              ""
            )}
            <input
              className={`${fNameData.isValid === false ? "-false" : ""}`}
              pattern="[A-Za-z]"
              type="text"
              value={fNameData.value}
              onChange={(e) => handleFnameData(e.target.value)}
              name={name}
              id={name}
              data-name={title ? title : ""}
            />
            {fNameData.isValid === false && (
              <div className="error-tooltip">
                <div className="error-text">{fNameData.errorText}</div>
              </div>
            )}
            {fNameData.isValid === true && <div className="valid-success" />}
          </div>
        );
      case "lname":
        return (
          <div className="Input input-wrapper">
            {title !== "" ? (
              <label className="input-title" htmlFor={name}>
                {title} {required ? <span>*</span> : ""}
              </label>
            ) : (
              ""
            )}
            <input
              className={`${lNameData.isValid === false ? "-false" : ""}`}
              pattern="[A-Za-z]"
              type="text"
              value={lNameData.value}
              onChange={(e) => handleLnameData(e.target.value)}
              name={name}
              id={name}
              data-name={title ? title : ""}
            />
            {lNameData.isValid === false && (
              <div className="error-tooltip">
                <div className="error-text">{lNameData.errorText}</div>
              </div>
            )}
            {lNameData.isValid === true && <div className="valid-success" />}
          </div>
        );
      case "email":
        return (
          <div className="Input input-wrapper">
            {title !== "" ? (
              <label className="input-title" htmlFor={name}>
                {title} {required ? <span>*</span> : ""}
              </label>
            ) : (
              ""
            )}
            <input
              className={`${emailData.isValid === false ? "-false" : ""}`}
              type="text"
              value={emailData.value}
              onChange={(e) => handleEmailData(e.target.value)}
              name={name}
              id={name}
              data-name={title ? title : ""}
            />
            {emailData.isValid === false && (
              <div className="error-tooltip">
                <div className="error-text">{emailData.errorText}</div>
              </div>
            )}
            {emailData.isValid === true && <div className="valid-success" />}
          </div>
        );
      case "pid":
        return (
          <div className="Input input-wrapper">
            {title !== "" ? (
              <label className="input-title" htmlFor={name}>
                {title} {required ? <span>*</span> : ""}
              </label>
            ) : (
              ""
            )}
            <input
              className={`${pidData.isValid === false ? "-false" : ""}`}
              type="text"
              value={pidData.value}
              onChange={(e) => handlePidData(e.target.value)}
              name={name}
              id={name}
              data-name={title ? title : ""}
            />
            {pidData.isValid === false && (
              <div className="error-tooltip">
                <div className="error-text">{pidData.errorText}</div>
              </div>
            )}
            {pidData.isValid === true && <div className="valid-success" />}
          </div>
        );
      case "phone":
        return (
          <div className="Input input-wrapper">
            {title !== "" ? (
              <label className="input-title" htmlFor={name}>
                {title} {required ? <span>*</span> : ""}
              </label>
            ) : (
              ""
            )}
            <div className="input-phone">
              <div
                className="selected -dropdown"
                onClick={() => handlePhoneClick()}
              >
                <div
                  className="flag"
                  style={{ backgroundPosition: selectedPhone.position }}
                />
                <div className="phone-index">{selectedPhone.num}</div>
                <div className="arrow" />
                {visiblePhone && (
                  <ul className="dropdown-content phone">
                    {phoneIndex.map((el, i) => (
                      <li
                        value={el.name}
                        key={i}
                        onClick={() =>
                          handlePhoneSelect({
                            name: el.name,
                            num: el.num,
                            short: el.short,
                            position: el.position,
                          })
                        }
                      >
                        <div
                          className={`flag -${el.short}`}
                          style={{ backgroundPosition: el.position }}
                        ></div>
                        <span className="country">{el.name}</span>
                        <span className="num">{el.num}</span>
                      </li>
                    ))}
                  </ul>
                )}
              </div>
              <input
                className={`${numData.isValid === false ? "-false" : ""}`}
                type="text"
                value={numData.value}
                onChange={(e) => handleNumData(e.target.value)}
                name={name}
                id={name}
                data-name={title ? title : ""}
              />
            </div>
            {numData.isValid === false && (
              <div className="error-tooltip">
                <div className="error-text">{numData.errorText}</div>
              </div>
            )}
            {numData.isValid === true && <div className="valid-success" />}
          </div>
        );
      case "date":
        return (
          <div className="Input input-wrapper input-date">
            {title !== "" ? (
              <label className="input-title" htmlFor={name}>
                {title} {required ? <span>*</span> : ""}
              </label>
            ) : (
              ""
            )}
            <div className="selection-wrappers">
              <select name="birthday_d" data-name="დაბადების თარიღი" id={name}>
                <option value="">რიცხვი</option>
                {options.day.map((el, i) => (
                  <option value={el} key={i}>
                    {el}
                  </option>
                ))}
              </select>
              <select name="birthday_m" data-name="დაბადების თარიღი">
                <option value="">თვე</option>
                {options.month.map((el, i) => (
                  <option value={el} key={i}>
                    {el}
                  </option>
                ))}
              </select>
              <select name="birthday_y" data-name="დაბადების თარიღი">
                <option value="">წელი</option>
                {options.year.map((el, i) => (
                  <option value={el} key={i}>
                    {el}
                  </option>
                ))}
              </select>
            </div>
          </div>
        );
      case "button":
        return (
          <div className="Input input-wrapper -button">
            <input
              type="button"
              value="რეგისტრაცია"
              className="reg-btn"
              onClick={() =>
                handleDataValidation(
                  emailData,
                  numData,
                  pidData,
                  lNameData,
                  fNameData,
                  pwData,
                  userData
                )
              }
            />
          </div>
        );
      default:
        return false;
    }
  };

  return <>{textInput()}</>;
};

export default Input;
