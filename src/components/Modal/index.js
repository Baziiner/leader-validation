import React from "react";
import "./Modal.scss";
import { handleModalClose } from "../../actions";
import { useDispatch } from "react-redux";

const Modal = () => {
  const dispatch = useDispatch();

  const closeModal = () => {
    dispatch(handleModalClose());
  };
  return (
    <div className="Modal">
      <div className="Modal-content">
        <p>რეგისტრაცია წარმატებულია</p>
        <button onClick={() => closeModal()}>დახურვა</button>
      </div>
    </div>
  );
};

export default Modal;
