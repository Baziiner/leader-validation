import React from 'react';
import './Header.scss';
import { ReactComponent as Logo } from '../../assets/images/leader.svg';

const Header = () => {
    return (
        <div className="Header">
            <div className="Header-content">
                <Logo className="logo" />
                <div className="market">
                    მარკეტი
                </div>
                <div className="auth-wrapper">
                    <div className="promotion" />
                    <div className="auth-elements">
                        <div className="auth-el">
                            <a href="#" className="register-button">
                                რეგისტრაცია
                            </a>
                        </div>
                        <div className="auth-el">
                            <input type="text" autoComplete="off" name="username" placeholder="სახელი" className="input icon-user" />
                            <div className="input_button">აღდგენა</div>
                        </div>
                        <div className="auth-el">
                            <input type="text" autoComplete="off" name="username" placeholder="პაროლი" className="input icon-password" />
                            <div className="input_button">აღდგენა</div>
                        </div>
                        <div className="auth-el">
                            <input type="button" value="შესვლა" className="login_btn" />
                        </div>
                        <div className="auth-el">
                            <div className="lang">
                                <div className="current_lang">&nbsp;</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Header;