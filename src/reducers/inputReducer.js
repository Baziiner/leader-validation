const initialState = {
  showModal: false,
  usename: {
    value: "",
  },
  password: {
    value: "",
  },
  email: {
    value: "",
  },
  dateData: {
    day: {
      value: "",
    },
    month: {
      value: "",
    },
    year: {
      value: "",
    },
  },
  phoneNum: {
    value: "",
  },
  fName: {
    value: "",
  },
  lName: {
    value: "",
  },
  pid: {
    value: "",
  },
  selectedFlag: {
    name: "Georgia (საქართველო)",
    short: "ka",
    position: "-176px 0",
  },
  selectedPhone: {
    name: "Georgia (საქართველო)",
    num: "+995",
    short: "ka",
    position: "-1819px 0px",
  },
  flagsVisible: false,
  phoneFlagsVisible: false,
  flags: [
    {
      name: "Georgia (საქართველო)",
      short: "ka",
      position: "-176px 0",
    },
    {
      name: "Turkey (Türkiye)",
      short: "tr",
      position: "-64px -44px",
    },
    {
      name: "Russia (Россия)",
      short: "ru",
      position: "-32px -88px",
    },
    {
      name: "Azerbaijan (Azərbaycan)",
      short: "az",
      position: "-144px -188px",
    },
    {
      name: "Armenia (Հայաստան)",
      short: "am",
      position: "-208px -55px",
    },
    {
      name: "Ukraine (Україна)",
      short: "ua",
      position: "-48px -44px",
    },
    {
      name: "Kazakhstan (Казахстан)",
      short: "kz",
      position: "-144px -44px",
    },
    {
      name: "Belarus (Беларусь)",
      short: "by",
      position: "-192px -88px",
    },
  ],
  flagsTel: [
    {
      name: "Georgia (საქართველო)",
      num: "+995",
      short: "ka",
      position: "-1819px 0px",
    },
    {
      name: "Turkey (Türkiye)",
      num: "+90",
      short: "tr",
      position: "-5065px 0px",
    },
    {
      name: "Russia (Россия)",
      num: "+7",
      short: "ru",
      position: "-4295px 0px",
    },
    {
      name: "Azerbaijan (Azərbaycan)",
      num: "+994",
      short: "az",
      position: "-352px 0px",
    },
    {
      name: "Armenia (Հայաստան)",
      num: "+374",
      short: "am",
      position: "-154px 0px",
    },
    {
      name: "Ukraine (Україна)",
      num: "+380",
      short: "ua",
      position: "-5175px 0px",
    },
    {
      name: "Kazakhstan (Казахстан)",
      num: "+7",
      short: "kz",
      position: "-2853px 0px",
    },
    {
      name: "Belarus (Беларусь)",
      num: "+375",
      short: "by",
      position: "-790px 0px",
    },
  ],
  date: {
    day: [
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      19,
      20,
      21,
      22,
      23,
      24,
      25,
      26,
      27,
      28,
      29,
      30,
      31,
    ],
    month: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
    year: [
      2002,
      2001,
      2000,
      1999,
      1998,
      1997,
      1996,
      1995,
      1994,
      1993,
      1992,
      1991,
      1990,
      1989,
      1988,
      1987,
      1986,
      1985,
      1984,
      1983,
      1982,
      1981,
      1980,
      1979,
      1978,
      1977,
      1976,
      1975,
      1974,
      1973,
      1972,
      1971,
      1970,
    ],
  },
};

const inputReducer = (state = initialState, action) => {
  switch (action.type) {
    case "SHOWFLAGS":
      return {
        ...state,
        flagsVisible: true,
      };
    case "HIDEFLAGS":
      return {
        ...state,
        flagsVisible: false,
      };
    case "SHOWPHONES":
      return {
        ...state,
        phoneFlagsVisible: true,
      };
    case "HIDEPHONES":
      return {
        ...state,
        phoneFlagsVisible: false,
      };
    case "SELECTCOUNTRY":
      return {
        ...state,
        selectedFlag: action.payload,
        flagsVisible: false,
      };
    case "SELECTPHONE":
      return {
        ...state,
        selectedPhone: action.payload,
        phoneFlagsVisible: false,
      };
    case "USER_FAIL":
      return {
        ...state,
        usename: action.payload,
      };
    case "USER_SUCCESS":
      return {
        ...state,
        usename: action.payload,
      };
    case "PW_FAIL":
      return {
        ...state,
        password: action.payload,
      };
    case "PW_SUCCESS":
      return {
        ...state,
        password: action.payload,
      };
    case "FNAME_FAIL":
      return {
        ...state,
        fName: action.payload,
      };
    case "FNAME_SUCCESS":
      return {
        ...state,
        fName: action.payload,
      };
    case "LNAME_FAIL":
      return {
        ...state,
        lName: action.payload,
      };
    case "LNAME_SUCCESS":
      return {
        ...state,
        lName: action.payload,
      };
    case "PID_FAIL":
      return {
        ...state,
        pid: action.payload,
      };
    case "PID_SUCCESS":
      return {
        ...state,
        pid: action.payload,
      };
    case "NUM_FAIL":
      return {
        ...state,
        phoneNum: action.payload,
      };
    case "NUM_SUCCESS":
      return {
        ...state,
        phoneNum: action.payload,
      };
    case "EMAIL_FAIL":
      return {
        ...state,
        email: action.payload,
      };
    case "EMAIL_SUCCESS":
      return {
        ...state,
        email: action.payload,
      };
    case "VALIDATION_SUCCESS":
      return {
        ...state,
        showModal: true,
      };
    case "MODAL_CLOSE":
      return {
        ...state,
        showModal: false,
      };
    default:
      return state;
  }
};

export default inputReducer;
